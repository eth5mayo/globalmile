import React, { useEffect } from "react";
import { Button, Text } from "react-native";

import { IdentityProvider } from "@bundly/ic-core-js";
import { useAuth, useClient, useCurrentProvider, useProviders } from "@bundly/ic-react";
import useParcelCanisterService from "../services/useParcelCanisterService";
import { ParamListBase, useNavigation } from "@react-navigation/native";
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import { Link, Redirect } from 'expo-router';
import GMButton from "./GMButton";

export type AuthButtonProps = {
  login?: {
    text?: string;
  };
};

export function AuthButton(props: AuthButtonProps) {
  const { isAuthenticated } = useAuth();
  return isAuthenticated ? <Redirect href={'/home/parcels'} /> : <LoginButton />;
}

export type LoginButtonProps = {
  title?: string;
};

function LoginButton(props: LoginButtonProps) {
  const client = useClient();
  const providers = useProviders();
  async function login() {
    try {
      const provider = selectProvider(providers);

      await client.setCurrentProvider(provider.name);

      await provider.connect();
    } catch (error) {
      await client.removeCurrentProvider();
    }
  }

  return <GMButton onPress={login} label={'Login'} />;
}

export type LogoutButtonProps = {
  title?: string;
};

function selectProvider(providers: IdentityProvider[]): IdentityProvider {
  if (providers.length === 0) {
    throw new Error("No providers available");
  }

  if (providers.length === 1) {
    return providers[0];
  }

  // TODO: Display view to select provider
  return providers[0];
}
