'use client';

import { IcpConnectContextProvider } from "@bundly/ic-react";
import { client } from "./ICPClient";
import { store, persistor } from '../redux/store'
import { Provider } from 'react-redux'
import { RouteChangeListener } from '../utils/route-change-listener';
import { PersistGate } from 'redux-persist/integration/react'

export function Providers({ children }: Readonly<{ children: React.ReactNode }>) {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <IcpConnectContextProvider client={client}>
            <RouteChangeListener>
              {children}
            </RouteChangeListener>
        </IcpConnectContextProvider>
      </PersistGate>  
    </Provider>
  );
}