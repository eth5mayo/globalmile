"use client"
import Sidebar from "@app/components/layout/Sidebar";

export default function Example({ children }: Readonly<{
    children: React.ReactNode;
  }>) {
  
  return (
    <>
      <Sidebar content={children} current="dashboard" />
    </>
  )
}