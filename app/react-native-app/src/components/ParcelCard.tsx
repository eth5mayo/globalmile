import React from 'react';
import { View, Text, Card } from 'react-native-ui-lib';
import { Parcel } from '../services/useParcelCanisterService'
import GMButton from './GMButton';

type ParcelCardProps = {
  parcel: Parcel
  onConfirm: () => void
  confirmTitle: string;
  borderColor?: string;
}

const ParcelCard = ({ parcel, onConfirm, confirmTitle, borderColor} : ParcelCardProps) => {
  
  return (
    <Card backgroundColor='white' marginV-10 padding-20 borderRadius={0} style={borderColor ? {
      borderRightWidth: 6,
      borderRightColor: borderColor
    }: {}} >
      <Text text70BO>{parcel.name}</Text>
      <Text>
        <Text text70BO>Dirección: </Text> 
        <Text text70L>{parcel.address}</Text>      
      </Text>
      <Text>
        <Text text70BO>Pago: </Text> 
        <Text text70L>${parcel.price} MXN</Text>      
      </Text>
      <GMButton size='small' label={confirmTitle} marginT-20 onPress={onConfirm} />
    </Card>
  );
};

export default ParcelCard;