"use client";

import { canisters } from "@app/canisters";
import { Client, InternetIdentity } from "@bundly/ic-core-js";
import { IcpConnectContextProvider } from "@bundly/ic-react";

export const client = Client.create({
    agent: {
      host: process.env.NEXT_PUBLIC_IC_HOST!,
    },
    canisters,
    providers: [new InternetIdentity({
      providerUrl: process.env.NEXT_PUBLIC_INTERNET_IDENTITY_URL,
    })]
});

