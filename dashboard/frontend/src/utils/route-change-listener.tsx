'use client';

import { setMe, setMyParcels } from '@app/redux/slices/user';
import { RootState } from '@app/redux/store';
import useUserCanisterService from '@app/services/useUserCanisterService';
import { useAuth } from '@bundly/ic-react';
import { AnonymousIdentity } from '@dfinity/agent';
import { redirect, usePathname } from 'next/navigation';
import { use, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const PROTECTED_PATHS = ['/dashboard', '/create-profile', '/parcels'];

export const RouteChangeListener = ({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) => {
  const pathname = usePathname();
  const { isAuthenticated, identity } = useAuth();
  const { getMe } = useUserCanisterService();
  const { me } = useSelector((state: RootState) => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    if (PROTECTED_PATHS.includes(pathname)) {
      if (!isAuthenticated) {      
        cleanData();  
        redirect('/signin');
      } else if (!me && pathname !== '/create-profile') {
        redirect('/create-profile');
      } else if (me && pathname === '/create-profile') {
        redirect('/dashboard');
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [me, isAuthenticated, pathname]);

  const cleanData = async () => {
    dispatch(setMe(null));
    dispatch(setMyParcels(null));
  }

  useEffect(() => {
    if (isAuthenticated && !me){
      getMe();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAuthenticated]);

  return <>{children}</>;
}