import React, { useEffect } from 'react';
import {
  Button,
  Image,
  Text,
  View,
} from 'react-native-ui-lib';
import Container from '../components/Container';
import { AuthButton } from '../components/auth-button';

const WelcomeScreen = () => {
  return (
    <Container safeArea centerH>
      <View flex-2>
        <Image
          source={require('../../assets/logo.png')}
          height={'100%'}
          resizeMode='contain'
        />
      </View>
      <View flex-3>
        <View center marginB-30>
          <Text text50BL marginB-30>Bienvenido</Text>
          <Text text80 center>Aplicación en donde se gestionarán tus necesidades para poder entregar paquetes de manera exitosa en todo el mundo</Text>
        </View>
        <AuthButton />
      </View>
    </Container>
  );
};

export default WelcomeScreen;
