"use client";

import ParcelStats, { ParcelStat } from "@app/components/stats/parcel-stats";
import { RootState } from "@app/redux/store";
import useParcelCanisterService from "@app/services/useParcelCanisterService";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

export default function Dashboard() {
    const [stats, setStats] = useState<ParcelStat[]>([])
    
    const { getMyParcels } = useParcelCanisterService();
    const { myParcels } = useSelector((state: RootState) => state.user);

    useEffect(() => {
        const totalParcels = myParcels?.length || 0;
        const totalDelivered = myParcels?.filter(parcel => parcel.status === 'delivered').length || 0;
        const totalPending = myParcels?.filter(parcel => parcel.status === 'pending').length || 0;

        setStats([
            {
                name: 'Envíos Creados',
                value: totalParcels
            },
            {
                name: 'Envíos Entregados',
                value: totalDelivered
            },
            {
                name: 'Envíos Pendientes',
                value: totalPending
            }
        ])
    }, [myParcels]);

    useEffect(() => {
        getMyParcels();
    }, []);

    return <>
        <ParcelStats stats={stats} />
    </>
  }
