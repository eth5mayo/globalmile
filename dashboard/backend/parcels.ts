import {Canister, Principal, Record, ic, None, Some, StableBTreeMap, Vec, bool, query, text, update, Opt} from 'azle';

const Parcel = Record({
    id: Principal,
    name: text,
    lat: text,
    lng: text,
    client: text,
    address: text,
    name_receiver: text,
    phone_receiver: text,
    status: text,
    price: text,
    user_id: Principal,
    driver_id: Opt(Principal)
})

type Parcel = typeof Parcel.tsType;

const parcels = StableBTreeMap<Principal, Parcel>(1);

export default Canister({
    createNewParcel: update([
        text,
        text,
        text,
        text,
        text,
        text,
        text,
        text
    ], bool, (
        name,
        lat,
        lng,
        client,
        address,
        name_receiver,
        phone_receiver,
        price
    ) => {
        const id = generatePrincipal();
        const user_id = ic.caller();

        const parcel: Parcel = {
            id,
            name,
            lat,
            lng,
            client,
            address,
            name_receiver,
            phone_receiver,
            status: 'pending',
            price,
            driver_id: None,
            user_id,
        };
        parcels.insert(id, parcel);

        return true;
    }),

    getMyEarnings: query([], text, () => {
        const myParcels = parcels.values()
            .filter(parcel => parcel.driver_id.Some?.toString() == ic.caller().toString())
            .filter(parcel => parcel.status == 'delivered');

        const earnings = myParcels.reduce((acc, parcel) => {
            return acc + parseFloat(parcel.price);
        }, 0);

        return earnings.toString();
    }),

    getParcelByStatus: query([text], Vec(Parcel), (status) => {
        return parcels.values().filter(parcel => parcel.status === status);
    }),

    getParcelByStatusByDriver: query([text], Vec(Parcel), (status) => {
        return parcels.values()
            .filter(parcel => parcel.status === status)
            .filter(parcel => parcel.driver_id?.Some.toString() == ic.caller().toString());
    }),

    setParcelStatus: update([text, text], bool, (parcel_id, status) => {
        const parcelId = Principal.fromText(parcel_id)
        const parcel = parcels.get(parcelId).Some;

        if (!parcel) throw new Error('No se encontro el paquete')

        parcel.status = status;
        console.log('parcel to update', parcel, parcelId)
        parcels.insert(parcelId, parcel);

        return true;
    }),

    assignParcelToDriver: update([text], bool, (parcel_id) => {
        const parcel = parcels.get(Principal.fromText(parcel_id)).Some;

        if (!parcel) throw new Error('No se encontro el paquete')

        parcel.driver_id = Some(ic.caller());
        parcel.status = 'confirmed';
        parcels.insert(Principal.fromText(parcel_id), parcel);

        return true;
    }),

    getAllParcels: query([], Vec(Parcel), () => {
        return parcels.values();
    }),

    getMyParcels: query([], Vec(Parcel), () => {
        return parcels.values().filter(parcel => parcel.driver_id.Some?.toString() == ic.caller().toString());
    }),

    getUserParcels: query([], Vec(Parcel), () => {
        return parcels.values().filter(parcel => parcel.user_id?.toString() == ic.caller().toString());
    })
});

function generatePrincipal(): Principal {
    const randomBytes = new Array(29)
        .fill(0)
        .map((_) => Math.floor(Math.random() * 256));

    return Principal.fromUint8Array(Uint8Array.from(randomBytes));
}
