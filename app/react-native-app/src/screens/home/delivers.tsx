import { useCallback, useEffect, useState } from "react";
import useParcelCanisterService, { Parcel } from "../../services/useParcelCanisterService";
import { useFocusEffect } from "expo-router";
import Container from "../../components/Container";
import { FlatList } from "react-native";
import ParcelCard from "../../components/ParcelCard";
import { Colors, Text, Card } from 'react-native-ui-lib'

export default function DeliversScreen() {
    const { getMyParcels, setParcelStatus } = useParcelCanisterService();  

    const [confirmedParcels, setConfirmedParcels] = useState<Parcel[]>([])
    const [loading, setLoading] = useState<boolean>(true);

    const getMyParcelsLocal = async () => {
        const parcels = await getMyParcels();
        setConfirmedParcels(parcels.filter((parcel) => parcel.status === 'confirmed'))
    }

    const fetchData = async () => {
      // Fetch data here
      setLoading(true);
      setTimeout(async () => {
        await getMyParcelsLocal()
        setLoading(false);
      }, 1000);
    };

    useFocusEffect(
        useCallback(() => {
          fetchData();
    
          return () => {
            // Clean up here
          };
        }, [])
      );

    const handleConfirmParcel = async (parcel: Parcel) => {
        setLoading(true)
        try {
          await setParcelStatus(parcel.id.toString(), 'delivered');
        } catch (e) {
          console.log({ e })
        }
        fetchData();
    }

    return (
    <Container paddingT-20 loading={loading}>
        <Text center marginB-20>
          <Text text60BL>Entregas disponibles: {confirmedParcels.length}</Text>          
        </Text>
        <FlatList 
            data={confirmedParcels}
            renderItem={({ item }) => (
                <ParcelCard borderColor={Colors.$textPrimary} confirmTitle="Entregar" parcel={item as Parcel} onConfirm={() => handleConfirmParcel(item)} />
            )}
            ListEmptyComponent={() => (
              <Card center borderRadius={0} padding-35 marginT-30>
                <Text text30BO center>No tienes paquetes por entregar</Text>
              </Card>)}
        />
    </Container>)
}