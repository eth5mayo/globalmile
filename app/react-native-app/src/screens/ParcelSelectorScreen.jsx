import React from 'react';
import {
  View, Text, TextField, Button,
} from 'react-native-ui-lib';
import { AuthButton } from '../components/auth-button';

export const ParcelSelectorScreen = () => {
  return (
    <View flex paddingH-25 paddingT-120>
      <Text blue50 text20>PARCEL</Text>
      <AuthButton />
    </View>
  );
};

export default ParcelSelectorScreen;
