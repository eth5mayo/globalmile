'use client';
import { useActor } from '@bundly/ic-react';
import { Actors } from '../canisters';
import type { Principal } from '@dfinity/principal';
import { useDispatch } from 'react-redux';
import { setMyParcels } from '@app/redux/slices/user';
import { Parcel } from '@app/app/parcels/page';

export type ParcelCanisterType = {
    id: Principal,
    name: string,
    lat: string,
    lng: string,
    client: string,
    address: string,
    name_receiver: string,
    phone_receiver: string,
    status: string,
    price: string,
    user_id: Principal,
    driver_id: [] | [Principal]
}

const useParcelCanisterService = () => {
    const parcelActor = useActor<Actors>('parcels') as Actors["parcels"];

    const dispatch = useDispatch();

    const createParcel = async (parcel: Parcel) => {
        const result = await parcelActor.createNewParcel(
            parcel.name,
            parcel.lat,
            parcel.lng,
            parcel.client,
            parcel.address,
            parcel.nameReceiver,
            parcel.phoneReceiver,
            parcel.price.toString()
        );
        console.log({result});
        return result;
    }

    const getAllParcels = async () => {
        const allParcels = await parcelActor.getAllParcels();

        return allParcels;
    }

    const getMyParcels = async () => {
        const myParcels = await parcelActor.getUserParcels();

        const parcels = myParcels.map((parcel: ParcelCanisterType): Parcel => {
            return {
                id: parcel.id,
                name: parcel.name,
                lat: parcel.lat,
                lng: parcel.lng,
                client: parcel.client,
                address: parcel.address,
                nameReceiver: parcel.name_receiver,
                phoneReceiver: parcel.phone_receiver,
                status: parcel.status,
                price: Number(parcel.price),
                userId: parcel.user_id,
                driverId: parcel.driver_id
            }
        });

        dispatch(setMyParcels(parcels));
        return parcels;
    }

    return {
        getAllParcels,
        createParcel,
        getMyParcels,
    }
};

export default useParcelCanisterService;
