'use client';
import { useActor } from '@bundly/ic-react';
import { Actors } from '../canisters';
import type { Principal } from '@dfinity/principal';

export type Parcel = {
  id: Principal;
  name: string;
  lat: string;
  lng: string;
  client: string;
  address: string;
  nameReceiver: string;
  phoneReceiver: string;
  status: string;
  price: number;
  userId?: Principal;
  driverId?: [] | [Principal]
};


export type ParcelCanisterType = {
    id: Principal,
    name: string,
    lat: string,
    lng: string,
    client: string,
    address: string,
    name_receiver: string,
    phone_receiver: string,
    status: string,
    price: string,
    user_id: Principal,
    driver_id: [] | [Principal]
}

const useParcelCanisterService = () => {
    const parcelActor = useActor<Actors>('parcels') as Actors["parcels"];

    const createParcel = async (parcel: Parcel) => {
        const result = await parcelActor.createNewParcel(
            parcel.name,
            parcel.lat,
            parcel.lng,
            parcel.client,
            parcel.address,
            parcel.nameReceiver,
            parcel.phoneReceiver,
            parcel.price.toString()
        );
        console.log({result});
        return result;
    }

    const getAllParcels = async () => {
        const allParcels = await parcelActor.getAllParcels();

        return allParcels;
    }

    const getMyParcels = async () => {
        const myParcels = await parcelActor.getMyParcels();

        const parcels = myParcels.map((parcel: ParcelCanisterType): Parcel => {
            return {
                id: parcel.id,
                name: parcel.name,
                lat: parcel.lat,
                lng: parcel.lng,
                client: parcel.client,
                address: parcel.address,
                nameReceiver: parcel.name_receiver,
                phoneReceiver: parcel.phone_receiver,
                status: parcel.status,
                price: Number(parcel.price),
                userId: parcel.user_id,
                driverId: parcel.driver_id
            }
        });
        return parcels;
    }

    const getParcelsByStatus = async (status: string) => {
        const myParcels = await parcelActor.getParcelByStatus(status);

        const parcels = myParcels.map((parcel: ParcelCanisterType): Parcel => {
            return {
                id: parcel.id,
                name: parcel.name,
                lat: parcel.lat,
                lng: parcel.lng,
                client: parcel.client,
                address: parcel.address,
                nameReceiver: parcel.name_receiver,
                phoneReceiver: parcel.phone_receiver,
                status: parcel.status,
                price: Number(parcel.price),
                userId: parcel.user_id,
                driverId: parcel.driver_id
            }
        });

        console.log('parcels', parcels)
        return parcels;
    }

    const asignParcelToDriver = async (parcelId: string) => {
        const result = await parcelActor.assignParcelToDriver(parcelId);
        return result
    }

    const setParcelStatus = async (parcelId: string, status: string) => {
        const result = await parcelActor.setParcelStatus(parcelId, status);
        return result
    }

    const getMyEarnings = async () => {
        const result = await parcelActor.getMyEarnings();
        return result
    }

    return {
        getAllParcels,
        createParcel,
        getMyParcels,
        getParcelsByStatus,
        asignParcelToDriver,
        setParcelStatus,
        getMyEarnings,
    }
};

export default useParcelCanisterService;
