"use client";
import React, { useEffect } from 'react';

import { AuthButton, useAuth } from "@bundly/ic-react";
import Image from 'next/image';
import { redirect } from 'next/navigation';

export default function Signin() {
  const { isAuthenticated } = useAuth();

  useEffect(() => {
    if (isAuthenticated) {
      redirect('/dashboard');
    }
  }, [isAuthenticated]);

    return (
        <>
      <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <Image
            className="mx-auto h-300 w-auto"
            src="../assets/logo-rectangle.svg"
            width={4000}
            height={1300}
            alt="Global Mile"
          />
          <h2 className="text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
            Inicia sesión con tu cuenta de Internet Identity
          </h2>
        </div>

        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
            <div style={{ textAlign: 'center' }}>
              <AuthButton loginButtonStyle={{ flex: 1, backgroundColor: 'blue', color: 'white', width: 200, padding: 10, alignContent: 'center' }} />
            </div>
        </div>
      </div>
    </>
    )
  }
