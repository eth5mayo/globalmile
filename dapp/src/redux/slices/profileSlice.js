import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  baseInformation: {
    name: "",
  }
};

export const counterSlice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    setBaseInformation: (state, action) => {
      state.baseInformation = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setBaseInformation } = counterSlice.actions;

export default counterSlice.reducer;