import { useAuth, useClient, useCurrentProvider, useProviders } from "@bundly/ic-react";
import { Redirect } from "expo-router";
import { Button, Text } from "react-native"
import GMButton from "../../components/GMButton";
import Container from "../../components/Container";

export default function AccountScreen() {
    const client = useClient();
    const provider = useCurrentProvider();
    const { isAuthenticated } = useAuth();

    if (!isAuthenticated) {
        return <Redirect href={'/'} />
    }

    async function logout() {
        if (!provider) {
          throw new Error("No identity provider selected");
        }
    
        try {
          await provider.disconnect();
          await client.removeCurrentProvider();
        } catch (error) {
          console.error(error);
          throw error;
        }
      }


    return (
    <Container centerV>
      <GMButton onPress={() => logout()} label="Cerrar Sesión" />
    </Container>)
}