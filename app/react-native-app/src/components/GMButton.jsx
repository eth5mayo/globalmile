import React from 'react';
import { View, Button } from 'react-native-ui-lib';

const GMButton = ({
  label,
  ...rest
}) => {
  return (
    <Button borderRadius={0} label={label} {...rest} />
  );
};

export default GMButton;