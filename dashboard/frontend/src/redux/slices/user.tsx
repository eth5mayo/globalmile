import { Parcel } from '@app/app/parcels/page'
import { User } from '@app/services/useUserCanisterService'
import { createSlice } from '@reduxjs/toolkit'

const initialState: {
  me: User | null
  myParcels: Parcel[] | null
} = {
  me: null,
  myParcels: null,  
}

export const userSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setMe: (state, action) => {
        state.me = action.payload
    },
    setMyParcels: (state, action) => {
      state.myParcels = action.payload
    }
  },
})

// Action creators are generated for each case reducer function
export const { setMe, setMyParcels } = userSlice.actions

export default userSlice.reducer