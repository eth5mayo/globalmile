import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import ParcelSelectorScreen from '../screens/ParcelSelectorScreen';

const Tab = createMaterialBottomTabNavigator();

const MainTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={ParcelSelectorScreen} />
    </Tab.Navigator>
  );
}

export default MainTabNavigator;