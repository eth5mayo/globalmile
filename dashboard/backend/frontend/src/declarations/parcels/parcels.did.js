export const idlFactory = ({ IDL }) => {
  return IDL.Service({
    'assignParcelToDriver' : IDL.Func([IDL.Text], [IDL.Bool], []),
    'createNewParcel' : IDL.Func(
        [
          IDL.Text,
          IDL.Text,
          IDL.Text,
          IDL.Text,
          IDL.Text,
          IDL.Text,
          IDL.Text,
          IDL.Text,
        ],
        [IDL.Bool],
        [],
      ),
    'getAllParcels' : IDL.Func(
        [],
        [
          IDL.Vec(
            IDL.Record({
              'id' : IDL.Principal,
              'lat' : IDL.Text,
              'lng' : IDL.Text,
              'status' : IDL.Text,
              'client' : IDL.Text,
              'name' : IDL.Text,
              'phone_receiver' : IDL.Text,
              'user_id' : IDL.Principal,
              'driver_id' : IDL.Opt(IDL.Principal),
              'address' : IDL.Text,
              'price' : IDL.Text,
              'name_receiver' : IDL.Text,
            })
          ),
        ],
        ['query'],
      ),
    'getMyEarnings' : IDL.Func([], [IDL.Text], ['query']),
    'getMyParcels' : IDL.Func(
        [],
        [
          IDL.Vec(
            IDL.Record({
              'id' : IDL.Principal,
              'lat' : IDL.Text,
              'lng' : IDL.Text,
              'status' : IDL.Text,
              'client' : IDL.Text,
              'name' : IDL.Text,
              'phone_receiver' : IDL.Text,
              'user_id' : IDL.Principal,
              'driver_id' : IDL.Opt(IDL.Principal),
              'address' : IDL.Text,
              'price' : IDL.Text,
              'name_receiver' : IDL.Text,
            })
          ),
        ],
        ['query'],
      ),
    'getParcelByStatus' : IDL.Func(
        [IDL.Text],
        [
          IDL.Vec(
            IDL.Record({
              'id' : IDL.Principal,
              'lat' : IDL.Text,
              'lng' : IDL.Text,
              'status' : IDL.Text,
              'client' : IDL.Text,
              'name' : IDL.Text,
              'phone_receiver' : IDL.Text,
              'user_id' : IDL.Principal,
              'driver_id' : IDL.Opt(IDL.Principal),
              'address' : IDL.Text,
              'price' : IDL.Text,
              'name_receiver' : IDL.Text,
            })
          ),
        ],
        ['query'],
      ),
    'getParcelByStatusByDriver' : IDL.Func(
        [IDL.Text],
        [
          IDL.Vec(
            IDL.Record({
              'id' : IDL.Principal,
              'lat' : IDL.Text,
              'lng' : IDL.Text,
              'status' : IDL.Text,
              'client' : IDL.Text,
              'name' : IDL.Text,
              'phone_receiver' : IDL.Text,
              'user_id' : IDL.Principal,
              'driver_id' : IDL.Opt(IDL.Principal),
              'address' : IDL.Text,
              'price' : IDL.Text,
              'name_receiver' : IDL.Text,
            })
          ),
        ],
        ['query'],
      ),
    'getUserParcels' : IDL.Func(
        [],
        [
          IDL.Vec(
            IDL.Record({
              'id' : IDL.Principal,
              'lat' : IDL.Text,
              'lng' : IDL.Text,
              'status' : IDL.Text,
              'client' : IDL.Text,
              'name' : IDL.Text,
              'phone_receiver' : IDL.Text,
              'user_id' : IDL.Principal,
              'driver_id' : IDL.Opt(IDL.Principal),
              'address' : IDL.Text,
              'price' : IDL.Text,
              'name_receiver' : IDL.Text,
            })
          ),
        ],
        ['query'],
      ),
    'setParcelStatus' : IDL.Func([IDL.Text, IDL.Text], [IDL.Bool], []),
  });
};
export const init = ({ IDL }) => { return []; };
