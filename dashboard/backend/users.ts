import {Canister, Principal, Record, ic, StableBTreeMap, Vec, bool, query, text, update} from 'azle';

const User = Record({
    id: Principal,
    username: text,
    client: text,
});

type User = typeof User.tsType;

const users = StableBTreeMap<Principal, User>(0);

export default Canister({

    getUser: query([], User, () => {
        const user = users.get(ic.caller()).Some

        if (!user) throw new Error('User not found')

        return user
    }),

    createNewUser: update([text, text], Principal, (username, client) => {
        const id = ic.caller();

        const userExists = users.get(ic.caller()).Some
        if (userExists) return user.id

        const user: User = {id, username, client};
        users.insert(id, user);

        return id
    }),

    getAllUsers: query([], Vec(User), () => {
        return users.values();
    })
});

function generatePrincipal(): Principal {
    const randomBytes = new Array(29)
        .fill(0)
        .map((_) => Math.floor(Math.random() * 256));

    return Principal.fromUint8Array(Uint8Array.from(randomBytes));
}
